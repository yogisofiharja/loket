class Config(object):
	DEBUG = True
	TESTING = False
	MYSQL_DATABASE_USER = 'root'
	MYSQL_DATABASE_PASSWORD = ''
	MYSQL_DATABASE_DB = 'loket'
	MYSQL_DATABASE_HOST = '127.0.0.1'

class DevelopmentConfig(Config):
	SECRET_KEY = 'Sementara123'

config = {
	'development' : DevelopmentConfig,
	'testing' : DevelopmentConfig,
	'production' : DevelopmentConfig
}