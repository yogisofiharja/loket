# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.21-MariaDB)
# Database: loket
# Generation Time: 2018-05-16 16:03:01 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table events
# ------------------------------------------------------------

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `locations_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `details` text,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;

INSERT INTO `events` (`id`, `users_id`, `locations_id`, `name`, `details`, `start`, `end`)
VALUES
	(1,1,1,'Ulang tahun loket nih','selamat ulang tahun nih','2018-05-16 10:00:00','2018-05-16 10:00:00'),
	(2,1,1,'kota kasablanka','perhelatan akbar','2018-05-16 10:00:00','2018-05-16 11:00:00'),
	(3,1,2,'test event','good music','2018-05-17 21:00:00','2018-05-16 22:00:00'),
	(4,2,2,'test 2','ini test 2','2018-05-20 10:00:00','2018-05-20 13:00:00');

/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table locations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `locations`;

CREATE TABLE `locations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(300) NOT NULL DEFAULT '',
  `address` text,
  `users_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;

INSERT INTO `locations` (`id`, `name`, `address`, `users_id`)
VALUES
	(1,'Gelora Bung Karno','jauh',1),
	(2,'the palace','SCBD',1),
	(3,'GOR Pemuda','pemuda pemudi',1);

/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table ticket_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ticket_categories`;

CREATE TABLE `ticket_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `events_id` int(11) DEFAULT NULL,
  `name` varchar(150) DEFAULT '',
  `price` bigint(20) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ticket_categories` WRITE;
/*!40000 ALTER TABLE `ticket_categories` DISABLE KEYS */;

INSERT INTO `ticket_categories` (`id`, `events_id`, `name`, `price`, `description`)
VALUES
	(4,2,'reguler',25000,'- 1 day entrance\r\n- free soft drink'),
	(5,2,'reguler 2',50000,'- 1 day entrance\r\n- free soft drink'),
	(6,1,'reguler',35000,'- 1 day entrance\r\n- free soft drink'),
	(7,4,'reguler',25000,'- 1 day entrance\r\n- free soft drink'),
	(8,4,'VIP',75000,'- 1 day entrance\r\n- free soft drink'),
	(11,3,'reguler',50000,'- 1 day entrance\r\n- free soft drink'),
	(12,1,'VIP',50000,'ada banyak uang disana.\r\ngo grab some');

/*!40000 ALTER TABLE `ticket_categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `twitter_id` varchar(20) DEFAULT NULL,
  `twitter_name` varchar(200) NOT NULL DEFAULT '',
  `twitter_screen_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `twitter_id`, `twitter_name`, `twitter_screen_name`)
VALUES
	(1,'99454649','Yogi Sofi Harja','YogiSofiHarja');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
