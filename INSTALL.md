this app used python 3.

assumed that we use virtualenv
- create database 'loket'
- import loket.sql
- virtualenv -p python3 env
- activate virtualenv
    source env/bin/activate
- install all requirement
    pip install -r requirements.txt
- python main.py