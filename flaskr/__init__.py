from flask import (
    Flask, current_app, Blueprint, flash, g, redirect, render_template, session, request, url_for
)
from flask_dance.contrib.twitter import make_twitter_blueprint, twitter
import requests, json, functools
from config import config

app = Flask(__name__)
with app.app_context():
    # within this block, current_app points to app.
    env = 'development'
    current_app.config.from_object(config[env])

    bp = make_twitter_blueprint(
        api_key="MBh0NknwELCpvQY8cYimU7KBJ",
        api_secret="nwdGnOT3jXVaHrWbECRpUnAGlDW2Kt5cJCYKLVMT90bgyDJ7HF",
        # login_url="/login",
        authorized_url="/oauth-authorized"
    )

    def login_required(view):
        """View decorator that redirects anonymous users to the login page."""

        @functools.wraps(view)
        def wrapped_view(**kwargs):
            if session.get('twitter_screen_name') is None:
                return redirect(url_for('signin'))

            return view(**kwargs)

        return wrapped_view

    from flaskr import events, locations

    current_app.register_blueprint(events.bp)
    current_app.register_blueprint(bp)
    current_app.register_blueprint(locations.bp)


    @current_app.route('/')
    def index():
        if session.get('twitter_oauth_token'):
            from flaskr.db import Users
            user = Users()
            resp = twitter.get('account/verify_credentials.json')
            resp = resp.text
            resp_data = json.loads(resp)
            params = {
                'twitter_id' : resp_data['id'],
                'twitter_name' : resp_data['name'],
                'twitter_screen_name' : resp_data['screen_name'],

            }
            user_id = user.get(params['twitter_id'])
            if not user_id:
                session['users_id'] = user.save(params)
            else:
                session['users_id'] = user_id['id']


            session['twitter_id'] = params['twitter_id']
            session['twitter_name'] = params['twitter_name']
            session['twitter_screen_name'] = params['twitter_screen_name']

        return redirect(url_for("events.index"))

    @current_app.route("/login")
    def signin():
        if not twitter.authorized:
            return render_template('auth/login.html')
        return redirect(url_for('events.index'))

    @current_app.route("/logout")
    def logout():
        session.clear()
        return redirect(url_for("index"))

    @current_app.route('/oauth-authorized')
    def oauth_authorized():
        access_token = requests.args.get('oauth_token')
        if access_token is None:
            next_url = request.args.get('next') or url_for('events.index')
            return redirect(next_url)

        session['access_token'] = access_token

        credential = twitter.get('account/verify_credentials.json')

        # session['screen_name'] = resp['screen_name']

        session['twitter_token'] = (
            # resp['oauth_token'],
            # resp['oauth_token_secret']
        )

        return redirect(url_for('events.index', data=credential))

