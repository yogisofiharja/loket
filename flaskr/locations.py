from flask import (
    Blueprint, flash, g, redirect, render_template, session, request, url_for
)
from flaskr.db import mysql, Users, Events, Locations
from werkzeug.exceptions import abort
from . import login_required
from pprint import pprint
from flask_dance.contrib.twitter import twitter
from urllib.parse import urlencode
import json
bp = Blueprint('locations', __name__, url_prefix='/locations')

@bp.route('/')

def index():
    loc = Locations()
    data = loc.mine()
    return render_template('locations/index.html', data=data)

@bp.route('/<int:id>/detail')
def get(id):
    loc = Locations()
    data = loc.get(id)
    return render_template('locations/detail.html', data=data)


@bp.route('/create', methods = ('GET', 'POST'))
@login_required
def create():
    cursor = mysql.get_db().cursor()
    if request.method == 'POST':
        name = request.form['name']
        address = request.form['address']

        error = ''
        if not name:
            if error != '':
                error += ', Location Name'
            else:
                error += 'Location Name'

        if not address:
            if error != '':
                error += ', Address'
            else:
                error += 'Address'


        if error is '' :
            params = {
                'name' : name,
                'address' : address,
                'users_id' : session.get('users_id')
            }

            loc = Locations()
            loc.save(params)
            return redirect(url_for('locations.index'))

        flash(error + ' is required')
    return render_template('locations/create.html')

@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    loc = Locations()
    curr_loc = loc.get(id)
    if request.method == 'POST':
        name = request.form['name']
        address = request.form['address']

        error = ''
        if not name:
            if error != '':
                error += ', Location Name'
            else:
                error += 'Location Name'

        if not address:
            if error != '':
                error += ', Address'
            else:
                error += 'Address'


        if error is '' :
            params = {
                'name' : name,
                'address' : address,
                'id' : id
            }

            loc.update(params)
            return redirect(url_for('locations.index'))

        flash(error + ' is required')
    return render_template('locations/update.html', data=curr_loc)

