from flask import (
    Blueprint, flash, g, redirect, render_template, session, request, url_for
)
from flaskr.db import mysql, Users, Events, Locations, Ticket
from werkzeug.exceptions import abort
from . import login_required
from pprint import pprint
from flask_dance.contrib.twitter import twitter
from urllib.parse import urlencode
import json
import dateutil.parser
from datetime import datetime
import locale
locale.setlocale(locale.LC_ALL, '')

bp = Blueprint('events', __name__, url_prefix='/events')

@bp.route('/')
# @login_required
def index():
    events = Events()
    data = events.all()
    return render_template('events/index.html', data=data)

@bp.route('/<int:id>/detail')
def get(id):
    events = Events()
    ticket = Ticket()
    data = events.get(id)
    d = dateutil.parser.parse(str(data['start']))
    date_start = d.strftime('%Y-%m-%d')
    time_start = d.strftime('%H:%M')
    d = dateutil.parser.parse(str(data['end']))
    date_end = d.strftime('%Y-%m-%d')
    time_end = d.strftime('%H:%M')
    data['date_start'] = date_start
    data['time_start'] = time_start
    data['date_end'] = date_end
    data['time_end'] = time_end
    data['tickets'] = ticket.all(id)


    return render_template('events/detail.html', data=data)


@bp.route('/create', methods = ('GET', 'POST'))
@login_required
def create():
    event = Events()
    loc = Locations()
    if request.method == 'POST':
        name = request.form['name']
        start = request.form['start']
        end = request.form['end']
        location = request.form.get('location')
        details = request.form['description']
        users_id = session.get('users_id')

        error = ''
        if not name:
            if error != '':
                error += ', Event Name'
            else:
                error += 'Event Name'
        if not start:
            if error != '':
                error += ', Event Time'
            else:
                error += 'Event Time'
        if not end:
            if error != '':
                error += ', Event Time'
            else:
                error += 'Event Time'
        if not location:
            flash('please add location first')
            return render_template('events/create.html', data=loc.all())
        if error is '' :
            params = {
                'name' : name,
                'start' : start,
                'end' : end,
                'locations_id' : location,
                'details' : details,
                'users_id' : users_id
            }
            event.save(params)
            return redirect(url_for('events.index'))

        flash(error + ' is required')

    return render_template('events/create.html', data=loc.all())

@bp.route('/<int:id>/update', methods = ('GET', 'POST'))
@login_required
def update(id):
    event = Events()
    if request.method == 'POST':
        name = request.form['name']
        start = request.form['start']
        end = request.form['end']
        location = request.form['location']
        details = request.form['description']
        users_id = session.get('users_id')

        error = ''
        if not name:
            if error != '':
                error += ', Event Name'
            else:
                error += 'Event Name'
        if not start:
            if error != '':
                error += ', Event Time'
            else:
                error += 'Event Time'
        if not end:
            if error != '':
                error += ', Event Time'
            else:
                error += 'Event Time'
        not_valid = ''
        start_date = datetime.strptime(start, '%Y-%m-%dT%H:%M')
        end_date = datetime.strptime(end, '%Y-%m-%dT%H:%M')
        if start_date > end_date :
            not_valid = 'End date should be greater than start date'

        if error is '' and not_valid is '':
            params = {
                'name' : name,
                'start' : start,
                'end' : end,
                'locations_id' : location,
                'details' : details,
                'users_id' : users_id,
                'id' : id
            }
            event.update(params)
            return redirect(url_for('events.index'))
        if error is not '' :
            flash(error + ' is required')
        if not_valid is not '':
            flash(not_valid)

    loc = Locations()
    ticket = Ticket()
    curr_events = event.get(id)

    d = dateutil.parser.parse(str(curr_events['start']))
    start = d.strftime('%Y-%m-%dT%H:%M')
    d = dateutil.parser.parse(str(curr_events['end']))
    end = d.strftime('%Y-%m-%dT%H:%M')
    curr_events['start']=start
    curr_events['end']=end
    data = {
        'event' : curr_events,
        'locations' : loc.all(),
        'ticket' : ticket.all(curr_events['id'])
    }

    return render_template('events/update.html', data=data)

@bp.route('/<int:id>/share')
def post_to_twitter(id):
    from flaskr.db import Events
    event = Events()
    data = event.get(id)
    status = "Please see my event \"{}\" here: {}".format(data['name'], request.url_root + 'events/' + str(id) + '/detail')
    result = twitter.post('statuses/update.json', data={'status' : status})
    resp = result.text
    resp_data = json.loads(resp)

    if 'errors' in resp_data:
        flash(resp_data['errors'][0]['message'])
    else:
        flash('Event shared to your timeline')
    return redirect(url_for('events.get', id=id))

@bp.route('/<int:id>/ticket', methods = ('GET', 'POST'))
def ticket_create(id):
    ticket = Ticket()
    if request.method == 'POST':
        name = request.form['name']
        price = request.form['price']
        description = request.form['description']

        error = ''
        if not name:
            if error != '':
                error += ', Ticket Name'
            else:
                error += 'Ticket Name'
        if not price:
            if error != '':
                error += ', Ticket Price'
            else:
                error += 'Ticket Price'

        if error == '':
            params = {
                'events_id' : id,
                'name': name,
                'price': price,
                'description': description
            }
            ticket.save(params)
            return redirect(url_for('events.update', id=id))

        if error is not '' :
            flash(error + ' is required')

    return render_template('events/create_ticket.html')

@bp.route('/<int:id>/ticket/<int:ticket_id>', methods = ('GET', 'POST'))
def ticket_update(id, ticket_id):
    ticket = Ticket()
    if request.method == 'POST':
        name = request.form['name']
        price = request.form['price']
        description = request.form['description']

        error = ''
        if not name:
            if error != '':
                error += ', Ticket Name'
            else:
                error += 'Ticket Name'
        if not price:
            if error != '':
                error += ', Ticket Price'
            else:
                error += 'Ticket Price'

        if error == '':
            params = {
                'id' : ticket_id,
                'name': name,
                'price': price,
                 'description': description
            }
            ticket.update(params)
            return redirect(url_for('events.update', id=id))

        if error is not '' :
            flash(error + ' is required')

    return render_template('events/update_ticket.html', data=ticket.get(ticket_id))

@bp.route('/<int:id>/ticket/<int:ticket_id>/delete', methods=('POST',))
def ticket_delete(id, ticket_id):
    ticket = Ticket()

    ticket.delete(ticket_id)
    return redirect(url_for('events.update', id=id))
