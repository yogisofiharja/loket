from flaskext.mysql import MySQL
from flaskr import current_app
from werkzeug.exceptions import abort
from pymysql.cursors import DictCursor
from flask import session

mysql = MySQL()
mysql.init_app(current_app)

class Users():
    def save(self, params):
        cursor = mysql.get_db().cursor()
        query = """
        insert into users(twitter_id, twitter_name, twitter_screen_name)
        SELECT * FROM (SELECT %(twitter_id)s, %(twitter_name)s, %(twitter_screen_name)s) AS tmp
        WHERE NOT EXISTS (
            SELECT * FROM users WHERE twitter_id= %(twitter_id)s
        ) LIMIT 1;
        """
        cursor.execute(query, params)
        mysql.get_db().commit()
        return cursor.lastrowid

    def get(self, twitter_id=None):
        db = mysql.get_db().cursor(DictCursor)

        query = """select id from users where twitter_id = {}""".format(twitter_id)
        db.execute(query)
        user = db.fetchone()
        if user is None:
            return False

        return user

class Events():
    def save(self, params):
        cursor = mysql.get_db().cursor()
        query = """insert into events(name, start, end, locations_id, details, users_id) values(%(name)s, %(start)s, %(end)s, %(locations_id)s, %(details)s, %(users_id)s)"""
        cursor.execute(query, params)
        mysql.get_db().commit()

    def update(self, params):
        cursor = mysql.get_db().cursor()
        query = """update events set name = %(name)s, start = %(start)s, end = %(end)s, locations_id = %(locations_id)s, details =%(details)s where id = %(id)s"""
        cursor.execute(query, params)
        mysql.get_db().commit()

    def get(self, id=None):
        db = mysql.get_db().cursor(DictCursor)
        twitter_id = session.get('twitter_id')
        query = """select e.*, l.name as location from events e, locations l where e.locations_id = l.id and e.id= {}""".format(id)
        db.execute(query)
        event = db.fetchone()
        if event is None:
            abort(404, "event doesn't exist.")

        return event

    def all(self):
        db = mysql.get_db().cursor(DictCursor)
        query = """select e.*, l.name as location from events e, locations l where e.locations_id = l.id"""
        db.execute(query)
        return db.fetchall()

    def mine(self):
        db = mysql.get_db().cursor(DictCursor)
        twitter_id = session.get('twitter_id')
        query = """select e.*, l.name as location from events e, locations l, users u where e.locations_id = l.id and e.users_id = u.id and u.twitter_id = {}""".format(session.get('twitter_id'))

        db.execute(query)
        return db.fetchall()

class Locations():
    def save(self, params):
        cursor = mysql.get_db().cursor()
        query = """insert into locations(name, address, users_id) values(%(name)s, %(address)s, %(users_id)s)"""
        cursor.execute(query, params)
        mysql.get_db().commit()

    def get(self, id=None):
        db = mysql.get_db().cursor(DictCursor)

        query = """select * from locations l where l.id= {}""".format(id)
        db.execute(query)
        loc = db.fetchone()
        if loc is None:
            abort(404, "location doesn't exist.")

        return loc

    def all(self):
        db = mysql.get_db().cursor(DictCursor)
        query = """select * from locations"""
        db.execute(query)
        return db.fetchall()

    def mine(self):
        db = mysql.get_db().cursor(DictCursor)
        twitter_id = session.get('twitter_id')
        query = """select l.* from locations l, users u where l.users_id = u.id and u.twitter_id = {}""".format(session.get('twitter_id'))

        db.execute(query)
        return db.fetchall()

    def update(self, params):
        cursor = mysql.get_db().cursor()
        query = """update locations set name = %(name)s,  address =%(address)s where id = %(id)s"""
        cursor.execute(query, params)
        mysql.get_db().commit()

class Ticket():
    def all(self, events_id):
        db = mysql.get_db().cursor(DictCursor)

        query = """select * from ticket_categories where events_id= {}""".format(events_id)
        db.execute(query)
        ticket = db.fetchall()

        return ticket

    def get(self, id):
        db = mysql.get_db().cursor(DictCursor)

        query = """select * from ticket_categories where id= {}""".format(id)
        db.execute(query)
        ticket = db.fetchone()

        return ticket

    def delete(self, id):
        db = mysql.get_db().cursor(DictCursor)
        query = """delete from ticket_categories where id= {}""".format(id)
        db.execute(query)
        mysql.get_db().commit()

    def update(self, params):
        cursor = mysql.get_db().cursor()
        query = """update ticket_categories set name = %(name)s, price =%(price)s, description =%(description)s where id = %(id)s"""
        cursor.execute(query, params)
        mysql.get_db().commit()

    def save(self, params):
        cursor = mysql.get_db().cursor()
        query = """insert into ticket_categories(name, price, events_id, description) values(%(name)s, %(price)s, %(events_id)s, %(description)s)"""
        cursor.execute(query, params)
        mysql.get_db().commit()